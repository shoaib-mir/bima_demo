import pandas as pd


class AgentID:
    
    def agentid(self):
        df = pd.read_excel('E:/BIMA_DEMO/agents.xlsx')
        return (df['WA ID'])

    def rowscount(self):
        df = pd.read_excel('E:/BIMA_DEMO/agents.xlsx')
        return (df.shape[0])

    def get_cloumn_data(self,id):
        df = pd.read_excel('E:/BIMA_DEMO/agents.xlsx')
        return df.iloc[:, id]

    def get_row_data(self,id):
        df = pd.read_excel('E:/BIMA_DEMO/agents.xlsx')
        return df.iloc[id]

    def get_cell_data(self,row,col):
        df = pd.read_excel('E:/BIMA_DEMO/agents.xlsx')
        return df.iloc[row,col]


All_Ids = AgentID()
print(All_Ids.rowscount())
