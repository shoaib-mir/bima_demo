*** Settings ***
Library  ImapLibrary
Library  SeleniumLibrary
Library  ExcelLibrary
Library  robot/libraries/String.py
Library  OperatingSystem
Library  BuiltIn
Library  Collections
Library  Libraries/DownloadAttachment.py
Library  Libraries/AgentID.py
Library  Libraries/Name.py
Library  Libraries/RowsCount.py
Library  ArchiveLibrary
Library  DateTime


*** Variables ***
${Browser}  Chrome
#${WAID}  WA1245
${password}  sms003
#${FullName}  shoaib mir
${username}  6666
${login}  Pak123
${Image1}  username.png
${Image2}  password.png
${URL}  http://6666:Pak123@63.141.240.125/
${digits}  1245
${caller_id}  9878
${phone_type}  ccagent
${download_path}  E:/BIMA_DEMO
#${path}  E:/RPA/agents.xlsx
${sheet_name}  Sheet1
${email} =  mercurialmindsrpa@gmail.com
${Emailpassword} =   QW12as34
${format} =  xlsx
${base_filename} =  Sheet1
${agentid} =  ${empty}
${fullname} =  ${empty}
${output_directory} =  Attachments
${directory_1} =  record
${directory} =  E:/BIMA_DEMO
${filename} =  E:/BIMA_DEMO/agents.xlsx
${TotalIterations} =  ${empty}

*** Keywords ***
Downloading Attachment
    # The code below will read the email and download the attachment with in that email
    [Arguments]  ${ExceFile}
    ${read_email_file} =  download_attachment  ${download_path}
    log to console  ${read_email_file}


Vicidial
    # The code below includes the whole process of getting the values from excel file and creating agents in vicidial

    # the code below will get all the values from the name column of excel attachment
    [Arguments]  ${index}
    log to console  printing names
    ${AgentName} =  agentname
    # ${AgentName} =  agentname  ${Index}  1
    #log to console  \n${AgentName}
    @{nameslist} =  convert to list  ${AgentName}
    :FOR  ${fullname}  IN  @{nameslist}
    \    log to console  \n${fullname}
    \    continue for loop if  '''${fullname}''' == '''${fullname}\[0]'''

    # This code below will get all the values of the ID column of excel attachmnet
    log to console  printing ids
    ${AgentID} =  agentid
    # ${AgentID} =  agentid  ${Index}  0
    #log to console  \n${AgentID}
    @{idslist} =  convert to list  ${AgentID}
    :FOR  ${agentid}  IN   @{idslist}
    \    log to console  \n$ ${agentid}
    \    continue for loop if  '''${agentid}''' == '''${agentid}\[0]'''


     #    log to console
     #    :FOR  ${agentid}  IN RANGE  0  @{idslist}
     #    \    continue for loop
     #    \    log to console  \n${agentid}

    # This code will open the vicidial panel and process of adding agents will start



    log to console  opening  browser
    open browser  ${URL}  ${browser}
    wait until page contains  Administration
    click element  //a[text()='Administration']
    click element  //font[text()='Users']
    click element  //font[text()='Add A New User ']
    click element  //input[@name='user']
    input text  //input[@name='user']  ${agentid}
    click element  //input[@name='pass']
    input text  //input[@name='pass']  ${password}
    click element  //input[@name='full_name']
    input text  //input[@name='full_name']  ${fullname}
    click element  //select[@name='user_level']
    click element  //option[text()='outboundgroup - OutBound Groupt']
    click element  //input[@name='phone_login']
    input text  //input[@name='phone_login']  ${agentid}
    click element  //input[@name='phone_pass']
    input text  //input[@name='phone_pass']  ${password}
    click element  //input[@type='button' and @name='SUBMIT']
    wait until page contains  MODIFY A USERS RECORD: ${agentid}
    click element  //input[@type='submit' and @name='SUBMIT']
    click element  //font[text()=' Admin ']
    click element  //span[text()=' Phones ']
    click element  //font[text()=' Add A New Phone ']
    input text  //input[@name='extension']  ${agentid}
    input text  //input[@name='dialplan_number']  ${agentid}
    input text  //input[@name='voicemail_id']  ${agentid}
    input text  //input[@name='outbound_cid']  ${caller_id}
    click element  //select[@name='user_group']
    click element  //option[text()='outboundgroup - OutBound Groupt']
    input text  //input[@name='login']  ${agentid}
    input text  //input[@name='phone_type']  ${phone_type}
    input text  //input[@name='fullname']  ${fullname}
    click element  //input[@type='submit' and @name='submit']
    wait until page contains  HOME
    click element  //input[@type='submit' and @name='submit']
    click element  //font[text()='Logout']
    close browser

Rename and Move Attachment
    # In this keyword i will rename the excel file which i downloaded from email and move it to another folder
    [Arguments]  ${movefile}
    create directory  ${output_directory}
    should exist  ${output_directory}
    ${directory_path} =  join path  ${output_directory}  ${directory_1}
    create directory  ${directory_path}
    should exist  ${directory_path}
    #${timestamp} =  set modified time  ${filename}  UTC + 1h 2min 3s
    ${timestamp} =  get current date  result_format=%d-%m-%Y-%H-%M-%S
    ${renamefile} =  move file  ${filename}  addedagents-${timestamp}.xlsx
    move files  ${renamefile}  ${directory_path}



*** Test Cases ***
Excel FIle
    # This test case will call download attachment  keyword to read and downlaod attachment
    Downloading Attachment  ${filename}



*** Test Cases ***
Adding Agents
    # This test case is used to count the total number of entries in excel file
    ${rowscount}=  rowscount
    log to console  ${rowscount}
    :FOR  ${TotalIterations}  IN RANGE  0  ${rowscount}
    \    Vicidial  ${TotalIterations}


*** Test Cases ***
Rename and move file
    # This test case will call rename and move attachment keyword
    rename and move attachment  ${directory_1}






